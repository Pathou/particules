using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System.Collections.Generic;
using Sprite;

namespace Particules
{
    public class GroupParticules
	{
		protected List<FrameSet> _frameSets;
		protected List<Particule> _particules;
		protected bool _emettre;
		protected double _timeNextParticule;

		protected double _NowTimeNextParticule;

		public double TimeNextParticule { get { return _timeNextParticule; } set { _timeNextParticule = 3600 / value; } }
		public Vector2 Pos { get; set; }
		public Vector2 Direction { get; set; }
		public float Speed { get; set; }
		public double Lifetime { get; set; }

		public int NbParticules { get { return _particules.Count; } }
		public List<Particule> Particules { get { return _particules; } }

		public GroupParticules(List<FrameSet> frameSets, int nbParticuleSec)
		{
			_frameSets = frameSets;
			_particules = new List<Particule>();
			TimeNextParticule = nbParticuleSec;
			_emettre = true;

			_NowTimeNextParticule = 0;

			Pos = Vector2.Zero;
			Direction = Vector2.Zero;
			Speed = 0;
			Lifetime = 1;
		}

		#region XNA

		public virtual void Update(GameTime gameTime)
		{
			if (_emettre)
			{
				_NowTimeNextParticule += gameTime.ElapsedGameTime.TotalMilliseconds;

				if (_NowTimeNextParticule > TimeNextParticule)
				{
					_NowTimeNextParticule = 0;
					this.AddParticule();
				}
			}

			_particules.ForEach(delegate(Particule item)
			{
				item.Update(gameTime);

				if (item.isKillable) _particules.Remove(item);
			});
		}

		public virtual void Draw(SpriteBatch spriteBatch, GameTime gameTime)
		{
			_particules.ForEach(delegate(Particule item)
			{
				item.Draw(spriteBatch, gameTime);
			});
		}

		#endregion

		#region M�thode

		/// <summary>
		/// Arr�ter l'�mission de particules
		/// </summary>
		public virtual void Stop()
		{
			_emettre = false;
		}

		/// <summary>
		/// D�marrer l'�mission de particules
		/// </summary>
		public virtual void Emettre()
		{
			_emettre = true;
		}

		/// <summary>
		/// Ajout d'une particule
		/// </summary>
		protected virtual void AddParticule()
		{
			_particules.Add(new Particule(_frameSets, Pos, Direction, Speed, Lifetime));
		}

		/// <summary>
		/// Ajout de particules
		/// </summary>
		public virtual void AddParticules()
		{

		}

		#endregion
	}
}
