using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System.Collections.Generic;
using Sprite;

namespace Particules
{
    public class Particule : SpriteAnimated
	{
		public Particule(List<FrameSet> frameSets, Vector2 pos, Vector2 direction, float speed, double maxLifetime)
			: base(frameSets)
		{
			Pos = pos;
			Direction = direction;
			Speed = speed;
			MaxLifetime = maxLifetime;
		}

		#region XNA

		public override void Update(GameTime gameTime)
		{
			base.Update(gameTime);
		}

		public override void Draw(SpriteBatch spriteBatch, GameTime gameTime)
		{
			base.Draw(spriteBatch, gameTime);
		}

		#endregion

		#region M�thode

		#endregion
	}
}
