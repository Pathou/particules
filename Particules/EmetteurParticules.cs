using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Particules
{
    /// <summary>
    /// Emetteur de particule
    /// </summary>
    public class EmetteurParticules
	{
		protected List<GroupParticules> _groups;
		protected bool _emettre;

		public List<GroupParticules> Groups { get { return _groups; } }
		public bool Emettre
		{
			get { return _emettre; }
			set
			{
				_groups.ForEach(delegate(GroupParticules item)
				{
					if(value)	item.Emettre();
					else		item.Stop();
				});
			}
		}

		public EmetteurParticules(List<GroupParticules> groups)
		{
			_groups = groups;
			_emettre = true;
		}

		#region XNA

		public void Update(GameTime gameTime)
		{
			if (_emettre)
			{
				_groups.ForEach(delegate(GroupParticules item)
				{
					item.Update(gameTime);
				});
			}
		}

		public void Draw(SpriteBatch spriteBatch, GameTime gameTime)
		{
			_groups.ForEach(delegate(GroupParticules item)
			{
				item.Draw(spriteBatch, gameTime);
			});
		}

		#endregion

		#region M�thode

		/// <summary>
		/// Modifie la position de tous les groupes
		/// </summary>
		/// <param name="pos"></param>
		public void SetPos(Vector2 pos)
		{
			_groups.ForEach(delegate(GroupParticules item)
			{
				item.Pos = pos;
			});
		}

		/// <summary>
		/// Stop l'emission de particule pour cet emetteur
		/// </summary>
		/// <param name="pos"></param>
		public void Stop()
		{
			_emettre = false;
		}

		#endregion

	}
}
